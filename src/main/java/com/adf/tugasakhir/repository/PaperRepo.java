package com.adf.tugasakhir.repository;


import com.adf.tugasakhir.dataclass.Paper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaperRepo extends JpaRepository<Paper, Long> {
    List<Paper> findByTitle(String title);
    List<Paper> findAllByOrderByIdAsc();
}
