# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

Name : Yafi Ahsan Hakim
NPM : 1806241242

TODO: Write the answers related to the programming exam here.

task no 1 (12 factors):
    
    1. Config : 
    The API key in application.properties file needs to be set as an 
    environment variable. to solve this, i added a new environment variable 
    in my intellij and gitlab repository with the name TEXT_ANALYTICS_API_KEY 
    and set the value to be the same as the one in the application.properties.
    i also added the same environment variable to my Gitlab environment variable.
    
    2. Logging:
    this factor means that we have to treat logs as event streams. there is one line
    in ScienceDirectPaperFinderTest that still uses System to print, so i change that 
    line from System.err.println(responseData.size()) to log.error(responseData.size()).  

notes: when i run the program locally it runs normally, but when i try to add a 
new conference in the deployed heroku app, eventhough the conference is added to 
the database, the view of the conferences somehow showed internal error, i am really 
confused because the one i run locally doesn't have the same problem, 
i actually don't know what to do :(