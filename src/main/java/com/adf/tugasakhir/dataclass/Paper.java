package com.adf.tugasakhir.dataclass;

import com.adf.tugasakhir.model.Conference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "paper")
@Getter @Setter @ToString
public class Paper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    @Column(name = "abstrak")
    private String abstrak;

    @NotNull
    @Column(name = "url")
    private String url;

    @ManyToOne
    private Conference conference;

    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }

    @Override
    public String toString() {
        return "Paper{" + "title='" + title + '\'' + ", abstrak='" + abstrak + '\'' + ", url='" + url + '\'' + '}';
    }
}
