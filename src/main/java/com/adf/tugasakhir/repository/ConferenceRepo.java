package com.adf.tugasakhir.repository;

import java.util.List;

import com.adf.tugasakhir.model.Conference;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ConferenceRepo
 */
public interface ConferenceRepo extends JpaRepository<Conference, Long> {
    List<Conference> findByNama(String nama);
    List<Conference> findAllByOrderByIdAsc();
}